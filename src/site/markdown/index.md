About BootConfig2adoc
=====================

```
    ____                  __    ______                   ____    _             ___                __              
   / __ )  ____   ____   / /_  / ____/  ____    ____    / __/   (_)   ____ _  |__ \  ____ _  ____/ /  ____   _____
  / __  | / __ \ / __ \ / __/ / /      / __ \  / __ \  / /_    / /   / __ `/  __/ / / __ `/ / __  /  / __ \ / ___/
 / /_/ / / /_/ // /_/ // /_  / /___   / /_/ / / / / / / __/   / /   / /_/ /  / __/ / /_/ / / /_/ /  / /_/ // /__  
/_____/  \____/ \____/ \__/  \____/   \____/ /_/ /_/ /_/     /_/    \__, /  /____/ \__,_/  \__,_/   \____/ \___/  
                                                                   /____/                                         
```


Spring-Boot-Configuration to AsciiDoc: AsciiDoctorJ-Extension to embed Spring-Boot configuration metadata files in AsciiDoc documents.


Documentation
-------------

| Name                                                  | Description                                                 |
|-------------------------------------------------------|-------------------------------------------------------------|       
| [Usage](./usage.html)                                 | How to use this AsciiDoctorJ-Extension                      |
| [User Guide (pdf)](./doc/asciidoc/pdf/user-guide.pdf) | More details about this AsciiDoctorJ-Extension for the user |

Example
-------

- [demo.html](./bootconfig2adoc-it/asciidoc/html/demo.html)
- [demo.pdf](./bootconfig2adoc-it/asciidoc/pdf/demo.pdf)
