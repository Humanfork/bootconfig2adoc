/*
 * Copyright 2019-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.humanfork.asciidoctorj.bootconfig2adoc.core;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.springframework.boot.configurationprocessor.metadata.ConfigurationMetadata;
import org.springframework.boot.configurationprocessor.metadata.JsonMarshaller;

public class ConfigurationMetadataReader {

	public ConfigurationMetadata read(final InputStream inputStream) throws IOException {
		Objects.requireNonNull(inputStream, "argument `inputStream` must not be null");

		org.springframework.boot.configurationprocessor.metadata.JsonMarshaller jsonMarshaller = new JsonMarshaller();
		try {
			return jsonMarshaller.read(inputStream);
		} catch (Exception e) {
			throw new IOException("Error while parsing inputstream");
		}
	}

	public ConfigurationMetadata read(final String fileName) throws IOException {
		Objects.requireNonNull(fileName, "argument `fileName` must not be null");

		Path file = Paths.get(fileName);
		try (InputStream inputStream = Files.newInputStream(file);) {
			return read(inputStream);
		} catch (Exception e) {
			throw new IOException("error while reading file `" + file + "`", e);
		}
	}
}
